/*

Various functions to create an audio description JSON file
with a set of raw segments from the Segment Annotator.

Main function is saveAudioDescJSON(raw_segs)

Created by Uri Nieto (oriol@nyu.edu)
Copyright 2014

*/


/* New methods to easily format the current date */
Date.prototype.today = function () { 
    return ((this.getDate() < 10)?"0":"") + this.getDate() +"/"+(((this.getMonth()+1) < 10)?"0":"") + (this.getMonth()+1) +"/"+ this.getFullYear();
}

Date.prototype.timeNow = function () {
    return ((this.getHours() < 10)?"0":"") + this.getHours() +":"+ ((this.getMinutes() < 10)?"0":"") + this.getMinutes() +":"+ ((this.getSeconds() < 10)?"0":"") + this.getSeconds();
}

/* Creates a data element, composed of value and confidence */
function DataElement(value, confidence) {
    this.value = value;
    this.confidence = confidence;
}

/* Annotation Object */
function Annotation(raw_segs) {
    var currentDate = new Date();

    // TODO: Variable metadata for the annotation
    this.name = "annotated_by_annotator";
    this.author = "Segment Annotator";
    this.group = "MARL";
    this.timestamp = currentDate.today() + " " + currentDate.timeNow();
    this.data = [];

    for (var i = 0; i < raw_segs.length; i++) {
        var raw_seg = raw_segs[i];
        var dataElement = {
            start: new DataElement(raw_seg.startTime, raw_seg.startConf),
            end: new DataElement(raw_seg.endTime, raw_seg.endConf),
            label: new DataElement(raw_seg.labelText, raw_seg.labelConf)
        };
        this.data.push(dataElement);
    }
}

/* Metadata Object */
function Metadata() {
    // TODO
    this.enid = "TR01924500";
    this.mbid = "4d4221a4-1c00-4ad3-9f7c-e0457623bad0";
    this.artist = "Michael Jackson";
    this.title = "Beat It";
    this.md5 = "63e5ee0470e210d88e7af816a03d2086";
    this.duration = "00:04:19";
    this.version = "0.1";
}

/*
Creates the section attribute based on the raw segments
*/
function createSectionAttribute(raw_segs) {
    var attribute = {};
    var sections = new Array();
    var annotation = new Annotation(raw_segs);

    // TODO: More than one annotation?
    sections[0] = annotation;

    // Only one attribute: sections
    attribute.sections = sections;

    // Create Metadata
    attribute.metadata = new Metadata();

    return attribute;
}

function saveJSON(jsData) {
    // Data to JSON format
    data = JSON.stringify(jsData);
    var encoded = btoa(data);

    // Send data over POST to be saved
    $.post('scripts/save_json.php', 'json=' + encoded + '&name=' + file_name, function(data,status){
        alert("Data: " + data + "\nStatus: " + status);
    });
}

function saveAudioDescJSON(raw_segs) {

    // Check if file exists
    $.ajax({
        url: './data/' + file_name + '-seg.json',
        dataType: 'json',
        error: function () {
            // file doesn't exist, let's create it from scratch
            var data = createSectionAttribute(raw_segs);

            // Save it
            saveJSON(data);
        },
        success: function(data) {
            // file exists, let's add a new anotation
            data.sections[1] = new Annotation(raw_segs);

            // Save it
            saveJSON(data);
        }
    });    
}