<?php
    
    /* This script saves a JSON file passed as POST into a local
    file in the server 

    Created by Uri Nieto (oriol@nyu.edu)
    */

    // Set segments directory
    $segdir = '../data/';

    // Set file name appending extension
    $segext = '-seg';
        
    // Counts how many "$name-seg" files exist in the $segdir dir
    function countSegs($name) {
        
        global $segdir, $segext;

        $i = 0; 
        $dir = dirname(__FILE__) . "/" . $segdir;
        if ($handle = opendir($dir)) {
            while (($file = readdir($handle)) !== false) {
                if (!in_array($file, array('.', '..')) && 
                    !is_dir($dir.$file) &&
                    strpos($file,$name . $segext) !== false) 
                        $i++;
            }
        }
        return $i;
    }

    // Saves the $data into $file
    function saveFile($file, $data) {
        if (!($jsonFile = fopen($file,'w'))) {
            die("Error: Couldn't create file: " . $file);
        }
        if (!fwrite($jsonFile, $data)) {
            die("Error: Couldn't write data into file: " . $file);
        }
        if (!fclose($jsonFile)) {
            die("Error: Couldn't close file: " . $file);
        }
    }

    // Get POST data
    $decoded = base64_decode($_POST['json']);
    $name = $_POST['name'];

    // Count number of annotated files:
    // $Nsegs = countSegs($name); // Not necessary with new JSON format

    // Save segments
    $outsegfile = $segdir . $name . $segext . '.json';
    saveFile($outsegfile, $decoded);

    // Success!
    echo "File Saved!";
?>