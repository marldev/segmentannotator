#!/usr/bin/env python
"""
This script uses Essentia in order to extract the beats of a given audio file
and save it into a JSON file.
"""

__author__ = "Oriol Nieto"
__copyright__ = "Copyright 2014, Music and Audio Research Lab (MARL)"
__license__ = "GPL"
__version__ = "1.0"
__email__ = "oriol@nyu.edu"

import argparse
import essentia
import essentia.standard
from essentia.standard import YamlOutput
import logging
import time

def process(audio_file, out_file, save_beats=False):
    """Main process."""
    
    # Load Audio
    logging.info("Loading Audio")
    loader = essentia.standard.MonoLoader(filename=audio_file)
    audio = loader()

    # Obtain beats
    logging.info("Obtaining Beats...")
    beattracker = essentia.standard.BeatTrackerMultiFeature()
    ticks, conf = beattracker(audio)

    # Save output as audio file
    if save_beats:
        logging.info("Saving Beats as an audio file")
        marker = essentia.standard.AudioOnsetsMarker(onsets=ticks, type='beep')
        marked_audio = marker(audio)
        essentia.standard.MonoWriter(filename='beats.wav')(marked_audio)

    # Save output as json file
    logging.info("Saving the JSON file")
    yaml = YamlOutput(filename=out_file, format='json')
    pool = essentia.Pool()
    pool.add("beats.ticks", ticks)
    pool.add("beats.confidence", conf)
    yaml(pool)

def main():
    """Main function to parse the arguments and call the main process."""
    parser = argparse.ArgumentParser(description=
        "Obtains the beats of a given file using Essentia and saves them " \
            "into a JSON file",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("audio_file",action="store",
        help="Input audio file")
    parser.add_argument("-o", action="store", dest="out_file", 
        default="output.json", help="Output JSON file")
    args = parser.parse_args()
    start_time = time.time()
   
    # Setup the logger
    logging.basicConfig(format='%(asctime)s: %(message)s', level=logging.INFO)

    # Run the algorithm
    process(args.audio_file, args.out_file)

    logging.info("Done! Took %.2f seconds." % (time.time() - start_time))

if __name__ == '__main__':
    main()