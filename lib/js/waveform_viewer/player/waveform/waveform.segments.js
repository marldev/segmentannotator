/**
 * WAVEFORM.SEGMENTS.JS
 *
 * This module handles all functionality related to the adding,
 * removing and manipulation of segments
 */

define([
  "m/bootstrap",
  "m/player/waveform/waveform.mixins"
  ], function (bootstrap, mixins) {

  return function (waveformView, options) {
    var self = this;

    self.uniqueSegments = 0; // Unique number of segments, even if some are deleted
    self.segments = [];
    self.views = [waveformView.waveformZoomView, 
      waveformView.waveformOverview].map(function(view){
        if (!view.segmentLayer) {
          view.segmentLayer = new Kinetic.Layer();
          view.stage.add(view.segmentLayer);
          view.segmentLayer.moveToTop();
        }
        return view;
      });

    var create_colors = function() {
      var colors = [];
      var r = 30, g = 30, b = 160;
      for (var i = 0; i < 26; i++) {
        var color = 'rgba('+r+', '+g+', '+b+', 1)';
        colors.push(color);
        r = (r+50) % 256;
        g = (g+10) % 256;
        b = (b+100) % 256;
      }

      return colors;
    };

    // Obtain colors
    self.colors = create_colors();

    // Function to compare segments to ease the sorting
    // Based on startTime. If it's the same, based on endTime.
    function compare(s1, s2) {
      if (s1.startTime < s2.startTime)
        return -1;
      if (s1.startTime > s2.startTime)
        return 1;
      if (s1.endTime < s2.endTime)
        return -1;
      if (s1.endTime > s2.endTime)
        return 1;
      return 0;
    }

    // Aligns the onset time into the closest beat in beats
    var align_beats = function (onset) {

      // If the beats are not set, return original onset
      if (self.beats.length === 0)
        return onset;

      var i;
      for (i = 0; i < self.beats.length; i++) {
        if (onset < self.beats[i]) {
          break;
        }
      }
      // if onset time is always greater than any beat time
      if (i == self.beats.length)
        return self.beats[i-1];

      // else, find closest beat time
      if ( self.beats[i] - onset < onset - self.beats[i-1] )
        return self.beats[i];
      else
        return self.beats[i-1];
    };

    // Set start time based on the rest of the segments:
    //  - start time will be the closest endtime to the current endtime
    var setStartTime = function (endTime) {
      if (self.segments.length === 0)
        return 0;

      var startTime = 0;
      for (var i = self.segments.length-1; i >= 0; i--) {
        if (self.segments[i].startTime < endTime && 
            self.segments[i].endTime < endTime) {
          startTime = self.segments[i].endTime;
          break;
        }
      }
      return startTime;
    };

    // Finds the linked segment (if any) to the segment passed as param
    // start: (bool) whether the marked dragged is the start or the end
    var findLinkedSegment = function (segment, start) {
      var linkedSegment = null;

      segIdx = self.segments.indexOf(segment);

      if (start && segIdx !== 0) {
        linkedSegment = self.segments[segIdx-1];
      }

      else if (!start && segIdx !== self.segments.length - 1) {
        linkedSegment = self.segments[segIdx+1];
      }

      return linkedSegment;
    };

    // Checks whether the segment is empty
    var isEmpty = function (startTime, endTime) {
      return (startTime >= endTime);
    };

    // Finds the beat index of a given time
    var findBeatIndex = function (time) {
      return self.beats.indexOf(time);
    };

    // Checks for the boundaries of the start or end of the song,
    // and updates accordingly if out of bounds
    var updateGlobalBounds = function (segment) {
      if (self.segments.indexOf(segment) === 0) {
        if (segment.startTime === undefined || segment.startTime < self.beats[0])
          segment.startTime = self.beats[0];
      }

      if (self.segments.indexOf(segment) === self.segments.length - 1) {
        if (segment.endTime === undefined || segment.endTime > self.beats[self.beats.length-1])
          segment.endTime = self.beats[self.beats.length-1];
      }
    };

    // Avoid segment overlap
    var avoidOverlap = function () {
      for (var i = 0; i < self.segments.length-1; i++) {
        var currSeg = self.segments[i];
        var nextSeg = self.segments[i+1];
        nextSeg.startTime = currSeg.endTime;
        updateSegmentWaveform(nextSeg);
      }
    };

    // Deselect all segments
    var deselectSegments = function () {
      for (var i = 0; i < self.segments.length; i++) {
        var seg = self.segments[i];
        // seg.zoom.waveformShape.attrs.fill = seg.color;
        // seg.overview.waveformShape.attrs.fill = seg.color;

        seg.overview.backRect.setFill(seg.color);
        seg.zoom.backRect.setFill(seg.color);
        seg.overview.backRect.setOpacity("0.1");
        seg.zoom.backRect.setOpacity("0.1");
        seg.selected = false;
      }
    };

    // Select a segment
    var selectSegmentGlob = function (segment) {

      deselectSegments();
      
      var opacity = 0.45;

      segment.overview.backRect.setFill("000000");
      segment.zoom.backRect.setFill("000000");
      segment.overview.backRect.setOpacity(opacity);
      segment.zoom.backRect.setOpacity(opacity);

      segment.selected = true;

      self.render();
    };

    // Update Segment Controls (HTML labels)
    var updateSegmentControls = function () {
      var segmentSelected = false;
      for (var i = 0; i < self.segments.length; i++) {
        var segment = self.segments[i];        
        if (segment.selected) {
          self.labelHTML.val(segment.labelText);
          self.labelConfHTML.val(segment.labelConf);
          self.startConfHTML.val(segment.startConf);
          self.endConfHTML.val(segment.endConf);
        }
      }
    };

    // Obtains the selected segment
    var getSelectedSegment = function() {
      for (var i = 0; i < self.segments.length; i++) {
        var segment = self.segments[i];
        if (segment.selected) return segment;
      }
      return null;
    };

    // Gets the color code based on a label. Returns null if not found
    var getColorFromLabel = function (label) {
      for (var i = 0; i < self.segments.length; i++) {
        var segment = self.segments[i];
        if (segment.labelText === label) return segment.color;
      }
      return null;
    };

    // Updates the selected label with the HTML value
    var updateLabel = function () {
      var segment = getSelectedSegment();

      // Update color accordingly
      segment.color = getColorFromLabel(self.labelHTML.val());

      // Update rec color
      segment.overview.waveformShape.setFill(segment.color);
      segment.zoom.waveformShape.setFill(segment.color);

      // Update label text
      segment.labelText = self.labelHTML.val();
      segment.zoom.label.setText(segment.labelText);
      segment.overview.label.setText(segment.labelText);

      self.render();
    };

    // Updates a specific confidence
    var updateConf = function (event) {
      var segment = getSelectedSegment();
      var seg_idx = self.segments.indexOf(segment);

      if (event.currentTarget.value > 1 || event.currentTarget.value < 0) {
        alert("Confidence values must be between 0 and 1");
        return;
      }

      switch(event.currentTarget.id) {
        case "labelconf":
          segment.labelConf = event.currentTarget.value;
          break;
        case "startconf":
          segment.startConf = event.currentTarget.value;
          if (seg_idx != 0)
            self.segments[seg_idx-1].endConf = event.currentTarget.value;
          break;
        case "endconf":
          segment.endConf = event.currentTarget.value;
          if (seg_idx != self.segments.length-1)
            self.segments[seg_idx+1].startConf = event.currentTarget.value; 
          break;
        default:
          console.log("error in segment control callback");
      }
    };

    // Finds the next available label
    var nextAvailableLabel = function () {

      for (var i = "A".charCodeAt(0); i <= "Z".charCodeAt(0); i++) {
        var found = false;
        for (var j = 0; j < self.segments.length; j++) {
          if (self.segments[j].labelText.charCodeAt(0) === i) {
            found = true;
            break;
          }
        }
        if (!found) {
          return String.fromCharCode(i);
        }
      }
      return currLabel;
    };

    // Delete Button is pressed: Delete the selected segment
    var deleteButtonPressed = function () {
      var segment = getSelectedSegment();

      if (segment !== null) {
        var segIdx = self.segments.indexOf(segment);
        self.segments.splice(segIdx, 1); // remove 1 element from array
        // Remove elements from the segmentLayers
        var viewIdx = self.views[1].segmentLayer.children.indexOf(segment.overview);
        self.views[0].segmentLayer.children[viewIdx].destroy(); // Zoomview
        self.views[1].segmentLayer.children[viewIdx].destroy(); // Overview

        // Avoid overlap (arrange segments)
        avoidOverlap();
      }
    };

    var createSegmentWaveform = function (segmentId, startTime, endTime, editable, 
        color, labelText, beatsync, beats) {
      
      self.beats = beats;

      var segment = {
        id: segmentId,
        startTime: beatsync ? align_beats(startTime) : startTime,
        endTime: beatsync ? align_beats(endTime) : endTime,
        labelText: labelText || nextAvailableLabel(),
        beatsync: beatsync
      };

      var segmentZoomGroup = new Kinetic.Group();
      var segmentOverviewGroup = new Kinetic.Group();

      var segmentGroups = [segmentZoomGroup, segmentOverviewGroup];

      color = color || getSegmentColor(segment);

      var menter = function (event) {
        this.parent.label.show();
        this.parent.view.segmentLayer.draw();
      };

      var mleave = function (event) {
        this.parent.label.hide();
        this.parent.view.segmentLayer.draw();
      };

      var selectSegment = function (event) {
        selectSegmentGlob(segment);
      };

      var dblClickOnWaveForm = function (event) {
        console.log("dbclick");
      };

      // For Zoom and Overview
      segmentGroups.forEach(function(segmentGroup, i) {
        var view = self.views[i];

        segmentGroup.waveformShape = new Kinetic.Shape({
          fill: color,
          strokeWidth: 0
        });

        // Always show the labels
        // segmentGroup.waveformShape.on("mouseenter", menter);
        // segmentGroup.waveformShape.on("mouseleave", mleave);
        segmentGroup.waveformShape.on("click", selectSegment);
        segmentGroup.waveformShape.on("dblclick", dblClickOnWaveForm);

        segmentGroup.add(segmentGroup.waveformShape);

        segmentGroup.label = new options.segmentLabelDraw(segmentGroup, segment);
        segmentGroup.add(segmentGroup.label);

        if (editable) {
          segmentGroup.inMarker = new options.segmentInMarker(true, segmentGroup, segment, segmentHandleDragStart);
          segmentGroup.add(segmentGroup.inMarker);

          segmentGroup.outMarker = new options.segmentOutMarker(true, segmentGroup, segment, segmentHandleDragEnd);
          segmentGroup.add(segmentGroup.outMarker);
        }

        // Create back rect
        var backRect = new Kinetic.Rect({
          height: options.height,
          fill: color,
          opacity: 0.1,
          stroke: 'none',
          strokeWidth: 4
        });

        segmentGroup.backRect = backRect;
        segmentGroup.backRect.on("click", selectSegment);
        segmentGroup.add(backRect);
        backRect.moveToBottom();
        // segmentGroup.waveformShape.moveToTop();

        view.segmentLayer.add(segmentGroup);
      });

      segment.zoom = segmentZoomGroup;
      segment.zoom.view = waveformView.waveformZoomView;
      segment.overview = segmentOverviewGroup;
      segment.overview.view = waveformView.waveformOverview;
      segment.color = color;
      segment.editable = editable;
      segment.selected = false;
      segment.startConf = 0.5;
      segment.endConf = 0.5;
      segment.labelConf = 0.5;

      return segment;
    };

    var updateSegmentWaveform = function (segment) {

      // Avoid overlap
      // avoidOverlap();

      // Check global boundaries
      updateGlobalBounds(segment);

      var newStartTime = segment.beatsync ? align_beats(segment.startTime) : segment.startTime;
      var newEndTime = segment.beatsync ? align_beats(segment.endTime) : segment.endTime;

      // Binding with data
      segment.startTime = newStartTime;
      segment.endTime = newEndTime;

      // Overview
      var overviewStartOffset = waveformView.waveformOverview.data.at_time(segment.startTime);
      var overviewEndOffset = waveformView.waveformOverview.data.at_time(segment.endTime);
      waveformView.waveformOverview.data.set_segment(overviewStartOffset, overviewEndOffset, segment.id);
      segment.overview.waveformShape.setDrawFunc(function(canvas) {
        mixins.waveformSegmentDrawFunction.call(this, waveformView.waveformOverview.data, segment.id, canvas, mixins.interpolateHeight(waveformView.waveformOverview.height));
      });

      segment.overview.setWidth(overviewEndOffset - overviewStartOffset);

      if (segment.editable) {
        segment.overview.backRect.show().setX(overviewStartOffset);
        segment.overview.backRect.show().setWidth(overviewEndOffset - overviewStartOffset);
        if (segment.overview.inMarker) segment.overview.inMarker.show().setX(overviewStartOffset - segment.overview.inMarker.getWidth());
        if (segment.overview.outMarker) segment.overview.outMarker.show().setX(overviewEndOffset);

        // Change Text
        segment.overview.inMarker.label.setText(mixins.niceTime(segment.startTime, false));
        segment.overview.outMarker.label.setText(mixins.niceTime(segment.endTime, false));
      }

      // Place the Label next to the start boundary
      segment.overview.label.setX(overviewStartOffset + 3);

      // Zoom
      var zoomStartOffset = waveformView.waveformZoomView.data.at_time(segment.startTime);
      var zoomEndOffset = waveformView.waveformZoomView.data.at_time(segment.endTime);
      waveformView.waveformZoomView.data.set_segment(zoomStartOffset, zoomEndOffset, segment.id);

      var frameStartOffset = waveformView.waveformZoomView.frameOffset;
      var frameEndOffset = waveformView.waveformZoomView.frameOffset + waveformView.waveformZoomView.width;

      if (zoomStartOffset < frameStartOffset) zoomStartOffset = frameStartOffset;
      if (zoomEndOffset > frameEndOffset) zoomEndOffset = frameEndOffset;

      if (waveformView.waveformZoomView.data.segments[segment.id].visible) {
        var startPixel = zoomStartOffset - frameStartOffset;
        var endPixel = zoomEndOffset - frameStartOffset;

        segment.zoom.show();
        segment.zoom.waveformShape.setDrawFunc(function(canvas) {
          mixins.waveformSegmentDrawFunction.call(this, waveformView.waveformZoomView.data, segment.id, canvas, mixins.interpolateHeight(waveformView.waveformZoomView.height));
        });

        if (segment.editable) {
          segment.zoom.backRect.show().setX(startPixel);
          segment.zoom.backRect.show().setWidth(endPixel - startPixel);
          if (segment.zoom.inMarker) segment.zoom.inMarker.show().setX(startPixel - segment.zoom.inMarker.getWidth());
          if (segment.zoom.outMarker) segment.zoom.outMarker.show().setX(endPixel);

          // Change Text
          segment.zoom.inMarker.label.setText(mixins.niceTime(segment.startTime, false));
          segment.zoom.outMarker.label.setText(mixins.niceTime(segment.endTime, false));
        }

        // Place the Label next to the start boundary
        segment.zoom.label.setX(startPixel + 10);

      } else {
        segment.zoom.hide();
      }

      

      // Label
      // segment.zoom.label.setX(0);
      // segment.zoom.label.setY(12);

      self.render();
    };

    var segmentHandleDragGeneric = function (thisSeg, segment, start) {
      // Get linked segment (null if none)
      var linkedSegment = findLinkedSegment(segment, start);
      var keyTime1 = start ? "startTime" : "endTime";
      var keyTime2 = !start ? "startTime" : "endTime";
      var minBeats = start ? 1 : -1;

      // Dragging start or end marker
      var offset = start ? 
        thisSeg.view.frameOffset + thisSeg.inMarker.getX() + thisSeg.inMarker.getWidth() :
        thisSeg.view.frameOffset + thisSeg.outMarker.getX();
      segment[keyTime1] = thisSeg.view.data.time(offset);

      // Update times
      segment[keyTime1] = segment.beatsync ? align_beats(segment[keyTime1]) : segment[keyTime1];
      if (isEmpty(segment.startTime, segment.endTime)) {
        segment[keyTime1] = segment[keyTime2];
        segment[keyTime1] = self.beats[findBeatIndex(segment[keyTime1])-minBeats];
      }

      // If linked segment, update it and check for empty segments
      if (linkedSegment !== null) {
        linkedSegment[keyTime2] = segment[keyTime1];
        if (isEmpty(linkedSegment.startTime, linkedSegment.endTime)) {
          linkedSegment[keyTime2] = linkedSegment[keyTime1];
          linkedSegment[keyTime2] = self.beats[findBeatIndex(linkedSegment[keyTime2])+minBeats];
          segment[keyTime1] = linkedSegment[keyTime2];
        }
        updateSegmentWaveform(linkedSegment);
      }

      updateSegmentWaveform(segment);
      self.render();
    };

    var segmentHandleDragStart = function (thisSeg, segment) {
      // Call the generic drag function
      segmentHandleDragGeneric(thisSeg, segment, true);
    };

    var segmentHandleDragEnd = function (thisSeg, segment) {
      // Call the generic drag function
      segmentHandleDragGeneric(thisSeg, segment, false);
    };

    // var getSegmentColor = function () {
    //   var c;
    //   if (options.randomizeSegmentColor) {
    //     var g = function () { return Math.floor(Math.random()*255); };
    //     c = 'rgba('+g()+', '+g()+', '+g()+', 1)';
    //   } else {
    //     c = options.segmentColor;
    //   }
    //   return c;
    // };

    var getSegmentColor = function (segment) {
      var c = self.colors[segment.labelText.charCodeAt(0) - 65];
      console.log(c);
      return c;
    };

    this.init = function () {
      bootstrap.pubsub.on("waveform_zoom_displaying", self.updateSegments);
    };

    /**
     * Update the segment positioning accordingly to each view zoom level and so on.
     *
     * Also performs the rendering.
     *
     * @api
     */
    this.updateSegments = function () {
      self.segments.forEach(updateSegmentWaveform);
      self.render();
    };

    /**
     * Manage a new segment and propagates it into the different views
     *
     * @api
     * @param {Number} startTime
     * @param {Number} endTime
     * @param {Boolean} editable
     * @param {String=} color
     * @param {String=} labelText
     * @return {Object}
     */
    this.createSegment = function (startTime, endTime, editable, color, labelText, beatsync, beats) {

      if (self.segments.length >= 26) {
        alert("Maximum number of segments reached!");
      }
      var segmentId = "segment" + self.uniqueSegments;

      // Setup startup time based on current endTime and previously created segments
      startTime = setStartTime(endTime);

      var segment = createSegmentWaveform(segmentId, startTime, endTime, editable, 
          color, labelText, beatsync, beats);

      self.segments.push(segment);
      self.segments.sort(compare);
      avoidOverlap();
      updateSegmentWaveform(segment);
      selectSegmentGlob(segment);

      self.uniqueSegments++;

      return segment;
    };

    /**
     * Performs the rendering of the segments on screen
     *
     * @api
     * @see https://github.com/bbcrd/peaks.js/pull/5
     * @since 0.0.2
     */
    this.render = function renderSegments(){
      updateSegmentControls();
      self.views.forEach(function(view){
        view.segmentLayer.draw();
      });
    };

    // Set the HTML values for the segment controls
    this.setSegmentControls = function (controls) {
      self.labelHTML = controls.find("#label");
      self.labelConfHTML = controls.find("#labelconf");
      self.startConfHTML = controls.find("#startconf");
      self.endConfHTML = controls.find("#endconf");
      self.deleteButton = controls.find("#deletesegment");

      // Init the labels
      var currLabel = 'A';
      for (var i = 0; i < 26; i++) {
        self.labelHTML.append(
          $("<option></option>")
          .attr("value", currLabel)
          .text(currLabel));  
        currLabel = String.fromCharCode((currLabel.charCodeAt(0) + 1));
      }

      // Set up callbacks
      self.labelHTML.change(updateLabel);
      self.labelConfHTML.change(updateConf);
      self.startConfHTML.change(updateConf);
      self.endConfHTML.change(updateConf);
      self.deleteButton.on("click", deleteButtonPressed);

    };
  };
});
