require.config({
  paths: {
    'm': "waveform_viewer"
  }
});

// Load jquery from global if present
if (window.$) {
  define('jquery', [], function() {
      return jQuery;
  });
} else {
  throw new Error("Peaks.js requires jQuery");
}

define([
  'm/bootstrap',
  'm/player/player',
  'm/player/waveform/waveform.core',
  'm/player/waveform/waveform.mixins',
  'templates/main',
  'm/player/player.keyboard',
  'jquery'
  ], function(bootstrap, AudioPlayer, Waveform, mixins, JST, keyboard, $){

  var buildUi = function () {
    return {
      $player: $("#waveform"),
      $zoom: $("#zoom-container"),
      $overview: $("#overview-container")
    };
  };

  var buildSegmentControl = function (segmentControls) {

    if (segmentControls !== null) {
      segmentControls.html('<p>' +
          'Label: <select id="label"></select><br/>' + 
          'Label Confidence: <input type="text" id="labelconf" size=3><br/>' +
          'Start Boundary Confidence: <input type="text" id="startconf" size=3><br/>' +
          'End Boundary Confidence: <input type="text" id="endconf" size=3><br/>' + 
          '<button id="addsegment">Add Segment</button>' +
          '<button id="deletesegment">Delete Segment</button><br/>' +
          '<button id="logsegments">Log Segments</button>' +
        '</p>');

      segmentControls.find('#addsegment').on("click", function (event) {
        endTime = peaks.time.getCurrentTime();
        segmentEditable = true;
        beatsync = true;
        peaks.segments.addSegment(0, endTime, segmentEditable, null, null, beatsync);
      });

      segmentControls.find('#logsegments').on("click", function (event) {
        var raw_segs = window.peaks.segments.getSegments();
        $.getScript("./scripts/write_audio_json.js", function() {
          saveAudioDescJSON(raw_segs);
        });
      });
    }
  };

  var api = { // PUBLIC API
    init: function (opts) {

      if (!opts.audioElement) {
        throw new Error("Please provide an audio element.");
      } else if (!opts.container) {
        throw new Error("Please provide a container object.");
      } else if (opts.container.width < 1 || opts.container.height < 1) {
        throw new Error("Please ensure that the container has a defined width and height.");
      }

      // Set up file
      $(opts.audioElement.id).html('<source src="./data/' + opts.fileName + 
          '.mp3" type="audio/mpeg"> Your browser does not support the audio element.');

      // Use space bar for start/stop
      $(document).keypress(function(e) {
          if(e.which == 32) {
            if ($(opts.audioElement.id)[0].paused) $(opts.audioElement.id)[0].play();
            else $(opts.audioElement.id)[0].pause();
          }
      });

      api.options = $.extend({
        zoomLevels: [512, 1024, 2048, 4096], // Array of scale factors (samples per pixel) for the zoom levels (big >> small)
        keyboard: false, // Bind keyboard controls
        nudgeIncrement: 0.01, // Keyboard nudge increment in seconds (left arrow/right arrow)
        inMarkerColor: '#a0a0a0', // Colour for the in marker of segments
        outMarkerColor: '#a0a0a0', // Colour for the out marker of segments
        zoomWaveformColor: 'rgba(0, 225, 128, 1)', // Colour for the zoomed in waveform
        overviewWaveformColor: 'rgba(0,0,0,0.2)', // Colour for the overview waveform
        randomizeSegmentColor: true, // Random colour per segment (overrides segmentColor)
        height: 200, // height of the waveform canvases in pixels
        segmentColor: 'rgba(255, 161, 39, 1)', // Colour for segments on the waveform,
        beats: [], // Empty beats
        segmentControls: null
      }, opts);

      api.options = $.extend({
        segmentInMarker: mixins.defaultInMarker(api.options),
        segmentOutMarker: mixins.defaultOutMarker(api.options),
        segmentLabelDraw: mixins.defaultSegmentLabelDraw(api.options)
      }, api.options);

      buildSegmentControl(api.options.segmentControls);

      api.currentZoomLevel = 0;

      $(api.options.container).html(JST.main).promise().done(function () {

        if (api.options.keyboard) keyboard.init();

        api.player = new AudioPlayer();
        api.player.init(api.options.audioElement);

        api.waveform = new Waveform();
        api.waveform.init(api.options, buildUi());

        window.peaks = api; // Attach to window object for simple external calls

        bootstrap.pubsub.on("waveformOverviewReady", function () {
          api.waveform.openZoomView();
          api.segments.setSegmentControls(api.options.segmentControls);
          if (api.options.segments) { // Any initial segments to be displayed?
            api.segments.addSegment(api.options.segments);
          }
        });
      });
    },

    segments: {
      addSegment: function (startTime, endTime, segmentEditable, color, labelText, beatsync) {
        if (typeof startTime === "number") {
          api.waveform.segments.createSegment(startTime, endTime, segmentEditable, 
                color, labelText, beatsync, api.options.beats);
        } else if (Array.isArray(startTime)){
          startTime.forEach(function(segment){
            api.waveform.segments.createSegment(segment.startTime, segment.endTime, segment.editable, 
                segment.color, segment.labelText, segment.beatsync, api.options.beats);
          });
        }

        // api.options.segmentControls.find("#label").val("1");
        // console.log(api.options.segmentControls.find("#labelconf").val("1)"));

        api.waveform.segments.render();
      },

      // removeSegment: function (segment) {

      // },

      // clearSegments : function () { // Remove all segments

      // },

      getSegments: function () {
        return api.waveform.segments.segments;
      },

      setSegmentControls: function (controls) {
        api.waveform.segments.setSegmentControls(controls);
      }

    },

    time: {
      getCurrentTime: function () {
        return api.player.getTime();
      }
    },

    zoom: { // namepsace for zooming related methods

      /**
       * Zoom in one level
       */
      zoomIn: function () {
        api.zoom.setZoom(api.currentZoomLevel - 1);
      },

      /**
       * Zoom out one level
       */
      zoomOut: function () {
        api.zoom.setZoom(api.currentZoomLevel + 1);
      },

      /**
       * Given a particular zoom level, triggers a resampling of the data in the zoomed view
       *
       * @param {number} zoomLevelIndex
       */
      setZoom: function (zoomLevelIndex) { // Set zoom level to index of current zoom levels
        if (zoomLevelIndex >= api.options.zoomLevels.length){
          zoomLevelIndex = api.options.zoomLevels.length - 1;
        }

        if (zoomLevelIndex < 0){
          zoomLevelIndex = 0;
        }

        api.currentZoomLevel = zoomLevelIndex;
        bootstrap.pubsub.emit("waveform_zoom_level_changed", api.options.zoomLevels[zoomLevelIndex]);
      },

      /**
       * Returns the current zoom level
       *
       * @returns {number}
       */
      getZoom: function () {
        return api.currentZoomLevel;
      }

    }
  };

  return api;

});
